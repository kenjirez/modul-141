# Marco Nemeth

Folgende Aufgaben wurden von Marco Nemeth aus der Klasse St20d gelöst.

## Aufgaben 4.3

### File Import "buchverwaltung.sql"

```
mysql -u krauthammer -p -D buchverwaltung < C:/Users/nemeth/Downloads/buchverwaltung.sql --default-character-set=utf8
```

### File Import "mehr_autoren.txt"

```
LOAD DATA LOCAL INFILE 'C:/Users/nemeth/Downloads/mehr_autoren.txt' 
INTO TABLE author 
CHARACTER SET UTF8
FIELDS TERMINATED BY '; '  
LINES TERMINATED BY '\n';
```

### File Import "noch_mehr_autoren.sql"

```
mysql -u krauthammer -p -D buchverwaltung < C:/Users/nemeth/Downloads/noch_mehr_autoren.sql --default-character-set=utf8
```

## Aufgaben 4.4

Alle erstellten Scripts sind unter ../Scripts abgelegt

### Script export "author" data to text File

```
SELECT * FROM author
INTO OUTFILE 'C:/temp/result.txt'
FIELDS TERMINATED BY ',' 
OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n';
```


