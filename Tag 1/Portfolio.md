# Marco Nemeth

Folgende Aufgaben wurden von Marco Nemeth aus der Klasse St20d gelöst.

### File Import "Abteilungen.txt"

```
LOAD DATA INFILE 'C:/Users/nemeth/Downloads/Firma/Abteilungen.txt' 
INTO TABLE tbl_abteilungen 
FIELDS TERMINATED BY ';' 
ENCLOSED BY '"' 
LINES TERMINATED BY '\n';
```

### File Import "Abteilungen2.txt"

```
LOAD DATA INFILE 'C:/Users/nemeth/Downloads/Firma/Abteilungen2.txt' 
INTO TABLE tbl_abteilungen 
CHARACTER SET UTF8 
FIELDS TERMINATED BY '; ' 
ENCLOSED BY '"' 
LINES TERMINATED BY '\n';
```

### File Import "tbl_plz_ort.sql"

```
mysql -u root -p -D firma < C:/Users/nemeth/Downloads/Firma/tbl_plz_ort.sql
```

### Table "tbl_plz_ort" delete Index

```
DROP INDEX plz_ort_ID ON tbl_plz_ort;
```

### Change "tbl_plz_ort" Engine

```
ALTER TABLE tbl_plz_ort ENGINE=InnoDB;
```

### Delete Content and insert CSV content into "tbl_plz_ort"

```
DELETE FROM tbl_plz_ort;
LOAD DATA INFILE 'C:/Users/nemeth/Downloads/Firma/plz_ort.csv' 
INTO TABLE tbl_plz_ort 
FIELDS TERMINATED BY ';' 
ENCLOSED BY '"' 
LINES TERMINATED BY '\n' 
IGNORE 1 ROWS;
```

### Foregin Key

```
```

###
